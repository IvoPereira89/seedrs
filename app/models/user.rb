class User < ApplicationRecord
  validates :first_name, presence: true
  validates :last_name, presence: true

  has_many :investments

  def self.whitelisted_attributes
    [:first_name, :last_name]
  end
end
