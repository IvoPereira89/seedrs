class Investment < ApplicationRecord
  validates :amount, presence: true
  validates :status, presence: true
  validates :campaign_id, presence: true

  attribute :status, default: 0
  enum status: [:processing, :completed, :cancelled, :voided]

  belongs_to :campaign
  belongs_to :user, optional: true

  scope :completed, -> { where(status: 1) }

  def process
    self.status = 0
    save!
  end

  def complete
    self.status = 1
    save!
  end

  def cancel
    self.status = 2
    save!
  end

  def void
    self.status = 3
    save!
  end

  def self.whitelisted_attributes
    [:amount, :campaign_id, :user_id, :status]
  end
end
