class Campaign < ApplicationRecord
  validates :name, presence: true
  validates :image, presence: true
  validates :target, presence: true
  validates :country, presence: true

  has_many :investments

  def can_invest?(value)
    invested + value <= target
  end

  def percentage_raised
    (invested / target) * 100
  end

  def invested
    investments.completed.sum(:amount)
  end

  def self.whitelisted_attributes
    [:name, :image, :target, :country]
  end
end
