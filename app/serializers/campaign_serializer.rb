class CampaignSerializer
  include FastJsonapi::ObjectSerializer
  attributes :name, :image, :target, :country, :percentage_raised
end
