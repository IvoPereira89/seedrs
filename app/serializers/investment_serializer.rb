class InvestmentSerializer
  include FastJsonapi::ObjectSerializer
  attributes :amount, :status, :user_id, :campaign_id
end
