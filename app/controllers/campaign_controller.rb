class CampaignController < ApplicationController
  def create
    begin
      campaign = CampaignsService::CreateCampaign.call(campaign_params)
    rescue Exception => e
      puts e.message
      render status: :bad_request, json: { error: 'Something went wrong. Try again later' } and return
    end

    render json: CampaignSerializer.new(campaign[:data]).serialized_json
  end

  def show
    begin
      campaign = CampaignsService::FetchCampaign.call(id: params[:id])
    rescue Exception => e
      puts e.message
      render status: :bad_request, json: { error: 'Something went wrong. Try again later' } and return
    end

    render json: CampaignSerializer.new(campaign[:data]).serialized_json
  end

  def index
    page = params[:page] || 1
    per_page = params[:per_page] || 25

    begin
      campaigns = CampaignsService::FetchCampaign.call(page: page.to_i, per_page: per_page.to_i)
    rescue Exception => e
      puts e.message
      render status: :bad_request, json: { error: 'Something went wrong. Try again later' } and return
    end

    render json: CampaignSerializer.new(campaigns[:data]).serialized_json
  end

  private

  def campaign_params
    params.permit(Campaign.whitelisted_attributes)
  end
end
