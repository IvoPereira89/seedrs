class InvestmentController < ApplicationController
  def create
    begin
      investment = InvestmentsService::CreateInvestment.call(investment_params)
    rescue Exception => e
      puts e.message

      message = 'Something went wrong. Try again later'
      message = e.message if e.message == 'Invalid investment amount'
      message = e.message if e.message == 'The invested value is bigger than amount left for the campaign target'

      render status: :bad_request, json: { error: message } and return
    end

    render json: InvestmentSerializer.new(investment[:data]).serialized_json
  end

  private

  def investment_params
    params.permit(Investment.whitelisted_attributes)
  end
end
