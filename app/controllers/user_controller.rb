class UserController < ApplicationController

  def create
    begin
      user = UsersService::CreateUser.call(user_params)
    rescue Exception => e
      puts e.message
      render status: :bad_request, json: { error: 'Something went wrong. Try again later' } and return
    end

    render json: UserSerializer.new(user[:data]).serialized_json
  end

  private

  def user_params
    params.permit(User.whitelisted_attributes)
  end
end
