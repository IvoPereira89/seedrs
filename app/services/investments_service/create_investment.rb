class InvestmentsService::CreateInvestment < ApplicationService
  def initialize(params)
    @investment_params = params
  end

  def call
    amount = @investment_params[:amount].to_i
    raise 'Invalid investment amount' and return if amount <= 0

    campaign = Campaign.find(@investment_params[:campaign_id])

    unless campaign.can_invest?(amount)
      raise 'The invested value is bigger than amount left for the campaign target' and return
    end

    ActiveRecord::Base.transaction do
      begin
        investment = Investment.create!(@investment_params)
        investment.complete
      rescue => exception
        raise ActiveRecord::Rollback
      end

      { data: investment }
    end
  end
end
