class ApplicationService
  def self.call(*args, &block)
    new(*args, &block).call
  end

  def client
    @client ||= "#{@service_name.capitalize}::Client".constantize.new
  end
end
