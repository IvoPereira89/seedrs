class CampaignsService::FetchCampaign < ApplicationService
  def initialize(params = {})
    @id = params[:id]
    @page = params[:page] || 1
    @per_page = params[:per_page] || 25
  end

  def call
    return { data: Campaign.find(@id) } if @id

    { data: Campaign.all.offset( (@page - 1) * @per_page).limit(@per_page) }
  end
end


