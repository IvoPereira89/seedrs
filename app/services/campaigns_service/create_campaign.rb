class CampaignsService::CreateCampaign < ApplicationService
  def initialize(params)
    @campaign_params = params
  end

  def call
    { data: Campaign.create!(@campaign_params) }
  end
end
