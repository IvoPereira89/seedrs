class UsersService::CreateUser < ApplicationService
  def initialize(params)
    @user_params = params
  end

  def call
    { data: User.create!(@user_params) }
  end
end
