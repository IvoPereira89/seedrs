# README

## Objective
This is a rails app that uses the Contenful API for Marley Spoon to retrieve recipes and show the details of each recipe

## Setup
* Extract the code or Checkout the project - `git clone git@gitlab.com:IvoPereira89/seedrs.git`
* Go into the directory - `cd seedrs`
* Install ruby 2.6.4 - (to check the current version try `ruby -v`) - IF NEEDED
* Run `bundle install`

## Restore DB - if needed
* Run `bundle exec rake db:drop db:setup`

## Run it
From inside the project directory:

* Run `bundle exec rails s`
The app should now be available on port `3000`

## Run tests
From inside the project directory:
* Run `bundle exec rspec spec/` - this will run all tests

If you need to run a specific test file do
* Run `bundle exec rspec {FILE_PATH}`

## Run examples
* With the project goes a POSTMAN collection ready to use to make it easier to run some examples

** Although the requests don't need a user, they are prepared for it.
To use just send the `user_id` on the Create Investment request
