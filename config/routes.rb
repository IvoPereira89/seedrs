Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  resources :user, only: [:create]
  resources :campaign, only: [:create, :show, :index]
  resources :investment, only: [:create]
end
