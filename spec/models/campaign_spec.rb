require 'rails_helper'

RSpec.describe Campaign, type: :model do
  describe '#name' do
    it 'validates presence of' do
      campaign = Campaign.new
      campaign.name = nil
      campaign.valid?
      campaign.errors[:name].should include("can't be blank")

      campaign.name = 'test name'
      campaign.valid?
      campaign.errors[:name].should_not include("can't be blank")
    end
  end

  describe '#image' do
    it 'validates presence of' do
      campaign = Campaign.new
      campaign.image = nil
      campaign.valid?
      campaign.errors[:image].should include("can't be blank")

      campaign.image = 'http://image.com'
      campaign.valid?
      campaign.errors[:image].should_not include("can't be blank")
    end
  end

  describe '#target' do
    it 'validates presence of' do
      campaign = Campaign.new
      campaign.target = nil
      campaign.valid?
      campaign.errors[:target].should include("can't be blank")

      campaign.target = 100000
      campaign.valid?
      campaign.errors[:target].should_not include("can't be blank")
    end
  end

  describe '#country' do
    it 'validates presence of' do
      campaign = Campaign.new
      campaign.country = nil
      campaign.valid?
      campaign.errors[:country].should include("can't be blank")

      campaign.country = 'PT'
      campaign.valid?
      campaign.errors[:country].should_not include("can't be blank")
    end
  end

  describe '#can_invest' do
    let!(:campaign) do
      Campaign.create!(
        name: 'test',
        image: 'http://test.image',
        target: 10000,
        country: 'UK'
      )
    end

    it 'allows for investment below target' do
      expect(campaign.can_invest?(100)).to be true
    end

    it "doesn't allow for invesment above target" do
      expect(campaign.can_invest?(10002)).to be false
    end
  end

  describe '#percentage_raised' do
    let!(:campaign) do
      Campaign.create!(
        name: 'test',
        image: 'http://test.image',
        target: 10000,
        country: 'UK'
      )
    end

    let!(:user) do
      User.create!(
        first_name: 'test',
        last_name: 'name'
      )
    end

    let!(:investment) do
      Investment.create!(
        amount: 5000,
        status: 'completed',
        campaign_id: campaign.id,
        user_id: user.id
      )
    end

    it 'shows the correct percentage raised' do
      expect(campaign.percentage_raised).to eq(50)
    end
  end

  describe '#invested' do
    let!(:campaign) do
      Campaign.create!(
        name: 'test',
        image: 'http://test.image',
        target: 10000,
        country: 'UK'
      )
    end

    let!(:user) do
      User.create!(
        first_name: 'test',
        last_name: 'name'
      )
    end

    let!(:investment) do
      Investment.create!(
        amount: 5000,
        status: 'completed',
        campaign_id: campaign.id,
        user_id: user.id
      )
    end

    let!(:investment2) do
      Investment.create!(
        amount: 2000,
        status: 'completed',
        campaign_id: campaign.id,
        user_id: user.id
      )
    end

    it 'calculates the correct invested amount' do
      expect(campaign.invested).to eq(7000)
    end
  end
end
