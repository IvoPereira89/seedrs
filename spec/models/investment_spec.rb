require 'rails_helper'

RSpec.describe Investment, type: :model do
  describe '#amount' do
    it 'validates presence of' do
      investment = Investment.new
      investment.amount = nil
      investment.valid?
      investment.errors[:amount].should include("can't be blank")

      investment.amount = 'test name'
      investment.valid?
      investment.errors[:amount].should_not include("can't be blank")
    end
  end

  describe '#campaign' do
    it 'validates presence of' do
      investment = Investment.new
      investment.campaign_id = nil
      investment.valid?
      investment.errors[:campaign_id].should include("can't be blank")

      investment.campaign_id = 1
      investment.valid?
      investment.errors[:campaign_id].should_not include("can't be blank")
    end
  end

  describe '#status' do
    let!(:campaign) do
      Campaign.create!(
        name: 'test',
        image: 'http://test.image',
        target: 10000,
        country: 'UK'
      )
    end

    let!(:investment) do
      Investment.create!(
        amount: 10,
        campaign_id: campaign.id
      )
    end

    it 'validates presence of' do
      investment.status = nil
      investment.valid?
      investment.errors[:status].should include("can't be blank")

      investment.status = 'processing'
      investment.valid?
      investment.errors[:status].should_not include("can't be blank")
    end

    it 'moves to processing' do
      investment.process
      expect(investment.status).to eq('processing')
    end

    it 'moves to completed' do
      investment.complete
      expect(investment.status).to eq('completed')
    end

    it 'moves to cancelled' do
      investment.cancel
      expect(investment.status).to eq('cancelled')
    end

    it 'moves to voided' do
      investment.void
      expect(investment.status).to eq('voided')
    end
  end
end
