require 'rails_helper'

RSpec.describe User, type: :model do
  describe '#first_name' do
    it 'validates presence of' do
      user = User.new
      user.first_name = nil
      user.valid?
      user.errors[:first_name].should include("can't be blank")

      user.first_name = 'Ze'
      user.valid?
      user.errors[:first_name].should_not include("can't be blank")
    end
  end

  describe '#last_name' do
    it 'validates presence of' do
      user = User.new
      user.last_name = nil
      user.valid?
      user.errors[:last_name].should include("can't be blank")

      user.last_name = 'Manel'
      user.valid?
      user.errors[:last_name].should_not include("can't be blank")
    end
  end
end
