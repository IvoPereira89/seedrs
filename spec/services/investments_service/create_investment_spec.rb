require 'rails_helper'

describe InvestmentsService::CreateInvestment do
  let!(:campaign) do
    Campaign.create!(
      name: 'test',
      image: 'http://image.test',
      target: 100,
      country: 'Portugal'
    )
  end

  let!(:user) do
    User.create!(
      first_name: 'TestFirst',
      last_name: 'TestLast'
    )
  end

  describe 'when called with correct parameters' do
    it 'creates a new investment on the campaign' do
      result = described_class.call(
        amount: 50,
        campaign_id: campaign.id,
        user_id: user.id
      )

      last_created = Investment.last
      expect(result[:data]).to eq(last_created)
      expect(last_created.amount).to eq(50)
      expect(last_created.campaign_id).to eq(campaign.id)
      expect(last_created.user_id).to eq(user.id)
      expect(campaign.percentage_raised).to eq(0.5)
      expect(campaign.invested).to eq(50)

    end
  end

  describe 'when called with wrong parameters' do
    it 'raises an exception' do
      expect { described_class.call(
        campaign_id: campaign.id,
        user_id: user.id
      ) }.to raise_error(RuntimeError)
    end
  end

  describe 'when called with amount bigget than target' do
    it 'raises an exception' do
      expect { described_class.call(
        amount: 120,
        campaign_id: campaign.id,
        user_id: user.id
      ) }.to raise_error(RuntimeError)
    end
  end
end
