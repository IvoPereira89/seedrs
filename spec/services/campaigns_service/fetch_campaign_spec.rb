require 'rails_helper'

describe CampaignsService::FetchCampaign do
  describe 'when called with id' do
    let!(:campaign) do
      Campaign.create!(
        name: 'test',
        image: 'http://image.test',
        target: 100,
        country: 'Portugal'
      )
    end
    it 'returns one' do
      result = described_class.call(id: campaign.id)
      expect(result[:data]).to eq(campaign)
    end

    describe 'when called with wrong id' do
      it 'raises an exception' do
        expect { described_class.call(
          id: campaign.id + 1
        ) }.to raise_error(ActiveRecord::RecordNotFound)
      end
    end
  end

  describe 'when called without id' do
    let!(:campaign_1) do
      Campaign.create!(
        name: 'test',
        image: 'http://image.test',
        target: 100,
        country: 'Portugal'
      )
    end

    let!(:campaign_2) do
      Campaign.create!(
        name: 'test2',
        image: 'http://image.test2',
        target: 200,
        country: 'Portugal'
      )
    end

    it 'returns all' do
      result = described_class.call
      expect(result[:data]).to eq([campaign_1, campaign_2])
    end

    it 'returns paginated results' do
      result = described_class.call(page: 2, per_page: 1)
      expect(result[:data]).to eq([campaign_2])
    end
  end
end
