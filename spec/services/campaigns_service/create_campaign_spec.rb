require 'rails_helper'

describe CampaignsService::CreateCampaign do
  describe 'when called with correct parameters' do
    it 'creates a new campaign' do
      result = described_class.call(
        name: 'test',
        image: 'http://image.test',
        target: 100,
        country: 'Portugal'
      )

      last_created = Campaign.last
      expect(result[:data]).to eq(last_created)
      expect(last_created.name).to eq('test')
      expect(last_created.image).to eq('http://image.test')
      expect(last_created.target).to eq(100)
      expect(last_created.country).to eq('Portugal')
    end
  end

  describe 'when called with wrong parameters' do
    it 'raises an exception' do
      expect { described_class.call(
        image: 'http://image.test',
        target: 100,
        country: 'Portugal'
      ) }.to raise_error(ActiveRecord::RecordInvalid)
    end
  end
end
