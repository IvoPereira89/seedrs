require 'rails_helper'

describe UsersService::CreateUser do
  describe 'when called with correct parameters' do
    it 'creates a new user' do
      result = described_class.call(
        first_name: 'test',
        last_name: 'last'
      )

      last_created = User.last
      expect(result[:data]).to eq(last_created)
      expect(last_created.first_name).to eq('test')
      expect(last_created.last_name).to eq('last')
    end
  end

  describe 'when called with wrong parameters' do
    it 'raises an exception' do
      expect { described_class.call(
        last_name: 'last'
      ) }.to raise_error(ActiveRecord::RecordInvalid)
    end
  end
end
