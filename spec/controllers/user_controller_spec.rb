require 'rails_helper'

RSpec.describe UserController, type: :controller do
  describe 'POST create' do
    it 'should create a user' do
      post :create, params: { first_name: 'test', last_name: 'name' }
      user = User.last


      expect(user.first_name).to eq('test')
      expect(user.last_name).to eq('name')
      expect(response.status).to eq(200)

      data = JSON.parse(response.body)['data']

      expect(data['id']).to eq(user.id.to_s)
      expect(data['attributes']['first_name']).to eq('test')
      expect(data['attributes']['last_name']).to eq('name')
    end

    it 'should not create a user when missing param' do
      post :create, params: { first_name: 'test'}

      expect(response.status).to eq(400)

      data = JSON.parse(response.body)['data']
      error = JSON.parse(response.body)['error']

      expect(data).to eq(nil)
      expect(error).to eq('Something went wrong. Try again later')
    end
  end
end
