require 'rails_helper'

RSpec.describe InvestmentController, type: :controller do
  describe 'POST create' do
    let!(:campaign) do
      Campaign.create!(
        name: 'test',
        image: 'url',
        target: 100,
        country: 'Portugal'
      )
    end
    let!(:user) do
      User.create!(
        first_name: 'test',
        last_name: 'last'
      )
    end

    it 'should create an investment' do
      post :create, params: {
        campaign_id: campaign.id,
        user_id: user.id,
        amount: 50
      }

      investment = Investment.last
      expect(investment.campaign.id).to eq(campaign.id)
      expect(investment.amount).to eq(50)
      expect(investment.status).to eq("completed")
      expect(campaign.percentage_raised).to eq(50)
    end

    it 'should not create an investment due to campaign target' do
      post :create, params: {
        campaign_id: campaign.id,
        amount: 101
      }

      expect(response.status).to eq(400)

      data = JSON.parse(response.body)['data']
      error = JSON.parse(response.body)['error']

      expect(data).to eq(nil)
      expect(error).to eq('The invested value is bigger than amount left for the campaign target')
    end

    it 'should not create an investment due to invalid amount' do
      post :create, params: {
        campaign_id: campaign.id,
        amount: -150
      }

      expect(response.status).to eq(400)

      data = JSON.parse(response.body)['data']
      error = JSON.parse(response.body)['error']

      expect(data).to eq(nil)
      expect(error).to eq('Invalid investment amount')
    end
  end
end
