require 'rails_helper'

RSpec.describe CampaignController, type: :controller do
  describe 'POST create' do
    it 'should create a campaign' do
      post :create, params: {
        name: 'test',
        image: 'http://test.image',
        target: 100,
        country: 'Portugal'
      }

      campaign = Campaign.last

      expect(campaign.name).to eq('test')
      expect(campaign.image).to eq('http://test.image')
      expect(campaign.target).to eq(100)
      expect(campaign.country).to eq('Portugal')

      expect(response.status).to eq(200)

      data = JSON.parse(response.body)['data']
      expect(data['attributes']['name']).to eq('test')
      expect(data['attributes']['image']).to eq('http://test.image')
      expect(data['attributes']['target']).to eq('100.0')
      expect(data['attributes']['country']).to eq('Portugal')
      expect(data['attributes']['percentage_raised']).to eq('0.0')
    end

    it 'should not create a campaign if param missing' do
      post :create, params: {
        image: 'http://test.image',
        target: 100,
        country: 'Portugal'
      }

      expect(response.status).to eq(400)

      data = JSON.parse(response.body)['data']
      error = JSON.parse(response.body)['error']

      expect(data).to eq(nil)
      expect(error).to eq('Something went wrong. Try again later')
    end
  end

  describe 'GET show' do
    let!(:campaign) do
      Campaign.create!(
        name: 'test',
        image: 'http://test.image',
        target: 100,
        country: 'Portugal'
      )
    end

    let!(:campaign_2) do
      Campaign.create!(
        name: 'test2',
        image: 'http://test.image2',
        target: 200,
        country: 'Portugal'
      )
    end

    it 'should return a campaign' do
      get :show, params: { id: campaign.id }

      expect(response.status).to eq(200)

      data = JSON.parse(response.body)['data']
      expect(data['attributes']).to eq({
        'name' => 'test',
        'image' => 'http://test.image',
        'target' => '100.0',
        'country' => 'Portugal',
        'percentage_raised' => '0.0'
      })
    end
  end

  describe 'GET index' do
    let!(:campaign_1) do
      Campaign.create!(
        name: 'test',
        image: 'http://test.image',
        target: 100,
        country: 'Portugal'
      )
    end

    let!(:campaign_2) do
      Campaign.create!(
        name: 'test2',
        image: 'http://test.image2',
        target: 200,
        country: 'Portugal2'
      )
    end

    it 'should return all campaigns' do
      get :index

      expect(response.status).to eq(200)

      data = JSON.parse(response.body)['data']
      expect(data).to eq([
        {
          'id' => campaign_1.id.to_s,
          'type' => 'campaign',
          'attributes' => {
            'name' => 'test',
            'image' => 'http://test.image',
            'target' => '100.0',
            'country' => 'Portugal',
            'percentage_raised' => '0.0'
          }
        }, {
          'id' => campaign_2.id.to_s,
          'type' => 'campaign',
          'attributes' => {
            'name' => 'test2',
            'image' => 'http://test.image2',
            'target' => '200.0',
            'country' => 'Portugal2',
            'percentage_raised' => '0.0'
          }
        }
      ])
    end

    it 'should return the second page of campaigns' do
      get :index, params: { page: 2, per_page: 1 }

      expect(response.status).to eq(200)

      data = JSON.parse(response.body)['data']
      expect(data).to eq([
        {
          'id' => campaign_2.id.to_s,
          'type' => 'campaign',
          'attributes' => {
            'name' => 'test2',
            'image' => 'http://test.image2',
            'target' => '200.0',
            'country' => 'Portugal2',
            'percentage_raised' => '0.0'
          }
        }
      ])
    end
  end
end
