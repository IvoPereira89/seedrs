class CreateCampaigns < ActiveRecord::Migration[6.0]
  def change
    create_table :campaigns do |t|
      t.string  :name, null: false
      t.string  :image, null: false
      t.decimal :target, null: false
      t.string  :country, null: false
      t.timestamps
    end
  end
end
