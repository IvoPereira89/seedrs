class CreateInvestments < ActiveRecord::Migration[6.0]
  def change
    create_table :investments do |t|
      t.decimal     :amount, null: false
      t.integer     :status, null: false
      t.belongs_to  :campaign, index: true, foreign_key: true
      t.belongs_to  :user, index: true, foreign_key: true
      t.timestamps
    end

    change_column_default(:investments, :status, 0)
  end
end
