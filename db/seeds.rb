# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Campaign.create!(
  name: 'Iron Man',
  image: 'http://warpzone.me/wp-content/uploads/2019/07/ironmanvrblogroll-1554180810643-696x392.jpg',
  target: 100000,
  country: 'United Kingdom'
)

Campaign.create!(
  name: 'Thor',
  image: 'https://static.quizur.com/i/b/59c12434e517b0.1645792359c12434c2cd94.33870136.jpg',
  target: 200000,
  country: 'Sweden'
)

Campaign.create!(
  name: 'Hulk',
  image: 'https://i.ytimg.com/vi/_HEpJ2fKdgM/maxresdefault.jpg',
  target: 300000,
  country: 'Australia'
)

Campaign.create!(
  name: 'Captain America',
  image: 'https://goombastomp.com/wp-content/uploads/2019/05/CAptain-America.jpg',
  target: 100000,
  country: 'United States of America'
)

Campaign.create!(
  name: 'Black Widow',
  image: 'https://akm-img-a-in.tosshub.com/indiatoday/images/story/201911/Black-Widow-Avengers-920x584-770x433.jpeg?JYSbESH.guHhjea4QNNd2Y4E0lhVO1i5',
  target: 100000,
  country: 'Russia'
)
